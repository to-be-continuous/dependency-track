# [1.7.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.6.0...1.7.0) (2025-02-12)


### Features

* allow tags creation on project ([280efa3](https://gitlab.com/to-be-continuous/dependency-track/commit/280efa36d075358de7947e4a3d49934a85df6806))

# [1.6.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.5.1...1.6.0) (2025-01-27)


### Features

* disable tracking service by default ([dc4db4b](https://gitlab.com/to-be-continuous/dependency-track/commit/dc4db4b66ec46f2849da1d948bcd751d2972427f))

## [1.5.1](https://gitlab.com/to-be-continuous/dependency-track/compare/1.5.0...1.5.1) (2024-10-04)


### Bug Fixes

* **release:** support full semantic-versioning specifcation (with prerelease and build metadata) ([aa813bc](https://gitlab.com/to-be-continuous/dependency-track/commit/aa813bc6eefe1d730374cf69b865dbe841e56ae5))

# [1.5.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.4.0...1.5.0) (2024-09-15)


### Features

* add quality gate feature ([58f99e6](https://gitlab.com/to-be-continuous/dependency-track/commit/58f99e6889f5c3bbd01d699639cfc79ff8d24924))

# [1.4.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.3.0...1.4.0) (2024-08-09)


### Features

* add risk score threshold input ([814a7f3](https://gitlab.com/to-be-continuous/dependency-track/commit/814a7f3f9a78867cb59d80f91fdc973be577fcff))

# [1.3.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.2.0...1.3.0) (2024-07-02)


### Features

* add show-findings toggle ([2fa8707](https://gitlab.com/to-be-continuous/dependency-track/commit/2fa87073b722b202d08cfd9fc33687359bfcc6f7))

# [1.2.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.1.0...1.2.0) (2024-06-30)


### Features

* add vault variant ([c1af8b6](https://gitlab.com/to-be-continuous/dependency-track/commit/c1af8b6ba905b22986eb5f504e7be2a3e40750b8))

# [1.1.0](https://gitlab.com/to-be-continuous/dependency-track/compare/1.0.0...1.1.0) (2024-06-13)


### Features

* add SBOM merge support ([cec9361](https://gitlab.com/to-be-continuous/dependency-track/commit/cec936123d0f6ea951c8a099d6a7cb2ed2208c70))

# 1.0.0 (2024-05-31)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([ee30d0c](https://gitlab.com/to-be-continuous/dependency-track/commit/ee30d0c55a74bb7d161f6dde019652fbfec92257))


### Features

* initial template ([3438762](https://gitlab.com/to-be-continuous/dependency-track/commit/3438762aa36402098a1fac9dd12661b799035e6e))
* **path:** default project path that handles the project unicity conttraint ([ea9d205](https://gitlab.com/to-be-continuous/dependency-track/commit/ea9d205f1841a4e194b8fddc017207337f428edf))
